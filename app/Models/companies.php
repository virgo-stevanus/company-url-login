<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class companies extends Model
{
    use HasFactory;
    // make protected 
    protected $table = 'companies';
    protected $primaryKey = 'id';
    protected $fillable = [
        'company_name',
        'address',
        'city',
        'created_users_id',
        'updated_users_id',
        'country'
    ];

    public function Remployee()
    {
        return $this->hasMany(employee::class);
    }
    
   

   

}
