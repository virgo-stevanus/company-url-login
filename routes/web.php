<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('company', Session::get('companyname') ?? 'company');
});

// Route login with if else
Route::get('/{company}/login', function(){
    if(Auth::check()){
        return redirect()->route('company', Session::get('companyname') ?? 'company');
    } else {
        return view('auth.login');
    }
})->name('origin');

Route::get('/company/login', function(){
    if(Auth::check()){
        return redirect()->route('company', Session::get('companyname') ?? 'company');
    } else {
        return view('auth.login');
    }
})->name('default');

Auth::routes();

Route::get('/{company}/employee', [HomeController::class, 'index'])->name('company');


// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
